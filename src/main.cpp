#include "WiFi.h"
#include <ETH.h> // quote to use ETH
#include "WebServer.h"
#include <Servo.h>

#define ETH_ADDR 1
#define ETH_POWER_PIN 16             // -1             // ??? Do not use it, it can cause conflict during the software reset.
#define ETH_POWER_PIN_ALTERNATIVE 16 // 17    // ???
#define ETH_MDC_PIN 23
#define ETH_MDIO_PIN 18
#define ETH_TYPE ETH_PHY_LAN8720
#define ETH_CLK_MODE ETH_CLOCK_GPIO17_OUT // ETH_CLOCK_GPIO0_IN // ???

// Published values for SG90 servos; adjust if needed
#define SERVO_SIGNAL_PIN 2
int minUs = 300;
int maxUs = 2800;
Servo servo;

WebServer server(80);
static bool eth_connected = false;

void setServoAngle(byte angle)
{
  // calculate angle to delayTime
  int delayTime = map(angle, 0, 180, minUs, maxUs);

  digitalWrite(SERVO_SIGNAL_PIN, HIGH);
  delayMicroseconds(delayTime);
  digitalWrite(SERVO_SIGNAL_PIN, LOW);
  delay(20); // short pause at the end of the regulating
}

void makeATurn()
{
  Serial.println("rotating");
  servo.write(180);
  delay(1000);
  for (int i = 180; i > 1; i--)
  {
    servo.write(i);
    delay(20);
  }
  server.send(200);
}

String SendHTML()
{
  String ptr = "<!DOCTYPE html> <html>\n";
  ptr += "<head><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\">\n";
  ptr += "<title>Storage Servo Controler</title>\n";
  ptr += "<style>html { font-family: Helvetica; display: inline-block; margin: 0px auto; text-align: left;}\n";
  ptr += "body{margin-top: 50px;} h1 {color: #444444;margin: 50px auto 30px;} h3 {color: #444444;margin-bottom: 30px;} footer {position:absolute; bottom:0; width:100%; height:60px; text-align:center}\n";
  ptr += "p {font-size: 17px;color: #000;margin-bottom: 10px;}\n";
  ptr += "</style>\n";
  ptr += "</head>\n";
  ptr += "<body>\n";
  ptr += "<h1>Storage Servo Controler</h1>\n";
  ptr += "<p>Welcome to the landing page of Storage Servo Controler. This API contains only one single endpoint which is described in Usage. Have fun!</p>\n";
  ptr += "<p>There is only a single entpoint: <code> /rotate </code> which triggers the servo to make a push move to the plate by: \n";
  ptr += "<ol> \n";
  ptr += "<li> rotating approximatly 90 degrees clockwise in a fast strike </li> \n";
  ptr += "<li> waits 1s </li>\n";
  ptr += "<li> returns slowly to its neutral position </li>\n";
  ptr += "</ol> \n";
  ptr += "<p> If you want to change this behaviour you have to adjust the <code>makeATurn()</code> function in the file src/main.cpp at this project. </p>\n";
  ptr += "<h2>Usage</h2>\n";
  ptr += "<p>Just paste this snipet in a terminal or in german windows: Eingabeaufforderung</p>\n";
  ptr += "<code>\n";
  ptr += "curl --request GET --url http://141.70.213.153:31080/rotate\n";
  ptr += "</code>\n";
  ptr += "<h2>Source</h2>\n";
  ptr += "<p>The following link is correct but just clicking on it would be to easy. Please copy and paste it in your browser. </p>\n";
  ptr += " <a href='https://gitlab.com/losredoe132/storage_servo_controler'>https://gitlab.com/losredoe132/storage_servo_controler</a> \n";
  ptr += "</body>\n";
  ptr += "<footer>\n";
  ptr += "2022        Finn Jonas Peper        CreatorBox2036\n";
  ptr += "</footer>\n";
  ptr += "</html>\n";
  return ptr;
}

void handle_NotFound()
{
  server.send(404, "text/plain", "Not found");
}

void handle_OnConnect()
{
  Serial.println("landing page requested");
  server.send(200, "text/html", SendHTML());
}

void WiFiEvent(WiFiEvent_t event) // strange WiFiEvent? we are wired?
{
  switch (event)
  {
  case SYSTEM_EVENT_ETH_START:
    Serial.println("ETH Started");
    // set eth hostname here
    ETH.setHostname("ESP32 Node Storage");
    break;
  case SYSTEM_EVENT_ETH_CONNECTED:
    Serial.println("ETH Connected");
    break;
  case SYSTEM_EVENT_ETH_GOT_IP:
    Serial.print("ETH MAC: ");
    Serial.print(ETH.macAddress());
    Serial.print(", IPv4: ");
    Serial.print(ETH.localIP());
    if (ETH.fullDuplex())
    {
      Serial.print(", FULL_DUPLEX");
    }
    Serial.print(", ");
    Serial.print(ETH.linkSpeed());
    Serial.println("Mbps");
    eth_connected = true;
    break;
  case SYSTEM_EVENT_ETH_DISCONNECTED:
    Serial.println("ETH Disconnected");
    eth_connected = false;
    break;
  case SYSTEM_EVENT_ETH_STOP:
    Serial.println("ETH Stopped");
    eth_connected = false;
    break;
  default:
    break;
  }
}

void setup()
{
  pinMode(ETH_POWER_PIN_ALTERNATIVE, OUTPUT);
  digitalWrite(ETH_POWER_PIN_ALTERNATIVE, HIGH);

  servo.attach(SERVO_SIGNAL_PIN);

  Serial.begin(115200);
  WiFi.onEvent(WiFiEvent);                                                               // WiFi ????
  ETH.begin(ETH_ADDR, ETH_POWER_PIN, ETH_MDC_PIN, ETH_MDIO_PIN, ETH_TYPE, ETH_CLK_MODE); // Enable ETH

  while (!((uint32_t)ETH.localIP()))
  {
  }; // Waiting for IP (leave this line group to get IP via DHCP)

  Serial.println(ETH.localIP());

  server.on("/rotate", makeATurn);
  server.on("/", handle_OnConnect);
  server.onNotFound(handle_NotFound);

  server.begin();
}

void loop()
{
  server.handleClient();
}